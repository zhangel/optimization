﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
    // Update is called once per frame
    void Update ()
    {
        Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();

        // Getting info of the raycast
        RaycastHit hit = new RaycastHit();

        // Player will now go toward position where the mouse was clicked
        if (Input.GetMouseButtonDown(0))

            // The player object locates and follows where the mouse in on the screen.
            if ( Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) )
        {

            GetComponent<NavMeshAgent>().destination = hit.point;

        }
	}
}
