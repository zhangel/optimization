﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour
{
    public float stonelife;

    // Destroys the prefab after X amount of seconds
    void Start()
    {
        Destroy(gameObject, stonelife);
    }
}