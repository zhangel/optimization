﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {   
        // Mouse controls for the stones 
        if (Input.GetButton("Fire1"))
        {   
            // States the direction of where the stones should be shooting
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition + Vector3.forward);
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            GameObject prefabInstance = GameObject.Instantiate(Prefab, this.transform.position, Quaternion.identity, null);
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }

    }

}
